import time
import digitalio
import board
led=digitalio.DigitalInOut(board.D13)
led.direction=digitalio.Direction.OUTPUT
D5=digitalio.DigitalInOut(board.D5)
D5.direction=digitalio.Direction.OUTPUT
A5=digitalio.DigitalInOut(board.A5)
A5.direction=digitalio.Direction.OUTPUT

while True:
    print("On")
    led.value=True
    D5.value=True
    A5.value=True
    time.sleep(3)
    print("Off")
    led.value=False
    D5.value=False
    A5.value=False
    time.sleep(3)

